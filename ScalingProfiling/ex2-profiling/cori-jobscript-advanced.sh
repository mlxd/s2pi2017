#!/bin/bash -l
#SBATCH -N 1
#SBATCH -t 10
#SBATCH -L SCRATCH
#SBATCH -C haswell
#SBATCH -J ex2a
##SBATCH --reservation=STPI_hsw

# In this exercise, we will run our TAU-instrumented miniFE job at a couple of
# scales and use TAU to identify which aspects of the code contributed most 
# to poor scaling. This version adds options for callpath profiling, and a few
# other features 

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-Training/s2pi2017/ScalingProfiling}

module load tau

# path to the miniFE executable we built:
ex=$HOME/$training_dir/ex2-profiling/build/miniFE-tau.x

# A 200x200x200 miniFE job should finish within 5 minutes on a single core 
# of a Cori Haswell node:
sz=200
cmd="$ex -nx $sz -ny $sz -nz $sz"

# A Cori Haswell node has 32 cores, each with 2 hyperthreads. Our Slurm 
# setup considers each hyperthread to be a CPU, but for this exercise we want
# each MPI task to have its own core. The following recipe calculates how
# many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
# a "cpu" in Slurm is a hyperthread:
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((SLURM_CPUS_ON_NODE/cpus_per_task))
max_mpi_ranks=$((SLURM_NNODES * max_mpi_per_node))

# we will still time the overall run, to get a sense of how much overhead 
# profiling added. We'll output the time in seconds to be consistent with ex1
TIMEFORMAT=%R

# a report of communication between each pair of ranks is interesting:
export TAU_COMM_MATRIX=1

# optional setting to try: collect a callpath profile:
#export TAU_CALLPATH=1
#export TAU_CALLPATH_DEPTH=10

# another optional setting to try: 
#export TAU_SAMPLING=1
#export TAU_EBS_PERIOD=10000    # default is 1000 (usec)

# you will notice that after each run, you have a file like  "profile.1.0.0" 
# for each MPI rank (and, if you used OpenMP, each OpenMP thread in each 
# MPI rank). With increasing core count this will quickly get out of hand - 
# you can set the following environment variable to make a single merged 
# profile file instead:
# (Note that paraprof can read .xml profile files, but the CLI-oriented pprof
# cannot)
#export TAU_PROFILE_FORMAT=merged

# let's look at the profile with 1, 2, 16 and 32 MPI processes:
for nranks in 1 2 16 32 ; do

  # make the run output easy to identify:
  label=tau_profile-sz${sz}-${SLURM_NNODES}n-${nranks}mpi-$SLURM_JOB_ID
  # run in a unique directory under $SCRATCH:
  rundir=$SCRATCH/$training_dir/ex2-profiling/$label
  mkdir -p $rundir
  cd $rundir

  echo "profiling miniFE with $nranks MPI processes on $SLURM_NNODES nodes at `date`"
  time srun -n$nranks -c$cpus_per_task --cpu_bind=cores $cmd > stdout 2> stderr

  # if your executable is build with dynamic linking you can use tau_exec to 
  # profile it at runtime without explicitly instrumenting it: 
  #time srun -n$nranks -c$cpus_per_task --cpu_bind=cores tau_exec -T MPI $cmd > stdout 2> stderr

  # paraprof has an option to compare profiles. The easiest way to use this is
  # to package each profile of interest into a single "packed profile" file, 
  # then start paraprof with: "paraprof file1.ppk file2.ppk". The following 
  # command packs the profile in the current directory into a single file:
  #paraprof --pack profile.${nranks}p.ppk 
done
echo "finished miniFE runs at `date`"
