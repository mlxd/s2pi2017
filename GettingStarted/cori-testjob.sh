#!/bin/bash -l
#SBATCH -N 2        # Use 2 nodes
#SBATCH -t 5        # Set 5 minute time limit
#SBATCH -L SCRATCH  # Job requires $SCRATCH file system
#SBATCH -C haswell  # use haswell nodes 

rundir=$SCRATCH/test_my_account/hello_cori.$SLURM_JOB_ID
mkdir -p $rundir
cd $rundir 

if [[ `pwd` != "$rundir" ]]; then
  echo "Something is wrong!"
fi

echo "running a simple command in `pwd`"
srun --label -n 5 -c 2 --cpu_bind=cores bash -c 'printf "%s\n" "hello from `uname -n`"' >> stdout
ls -l $rundir
cat stdout 
